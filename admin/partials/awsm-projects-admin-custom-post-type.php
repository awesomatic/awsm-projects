<?php

  // Set the labels, this variable is used in the $args array
  $labels = array(
    'name'               => __( 'Projecten' ),
    'singular_name'      => __( 'project' ),
    'add_new'            => __( 'Nieuw project' ),
    'add_new_item'       => __( 'Nieuw project' ),
    'edit_item'          => __( 'Edit project' ),
    'new_item'           => __( 'Nieuw project', 'awsm-projects' ),
    'all_items'          => __( 'Alle projecten', 'awsm-projects' ),
    'view_item'          => __( 'Bekijk project' ),
    'search_items'       => __( 'Zoek projecten' ),
    'featured_image'     => 'Img',
    'set_featured_image' => 'Voeg afbeelding toe'
  );

  // The arguments for our post type, to be entered as parameter 2 of register_post_type()
  $args = array(
    'labels'            => $labels,

    // The only way to read that field is using this code: $obj = get_post_type_object( 'your_post_type_name' ); echo esc_html( $obj->description );
    //'description'       => 'Type some description ..',

    // Whether to exclude posts with this post type from front end search results
    'public'            => true,

    // Check out all available icons: https://developer.wordpress.org/resource/dashicons/#layout
    'menu_icon'         => 'dashicons-text-page',

    // Options: 26 Comments 2 - Dashboard 4 - Seperator 5 - Posts 10 - Media 15 - Links 20 - Pages - 25 Comments - 
    // 59 Seperator - 60 Appaerence - 65 Plugins - 70 Users - 75 Tools - 80 Settings - 99 - Seperator - 200 Seperator - 205 Starter Theme - 215 projects - 300 Seperator
    'menu_position'     => 215,

    // 'comments' editor has to be true to make us of the Gutenberg editor ,'excerpt'
    'supports'          => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),

    'rewrite'            => array( 
        /* 
         * 'projects' Will be used as slug on the permalink 
         */
        'slug'              => 'projects', // Custom string. Just be aware of potential conflicts with other plugins or themes using the same slug.
        /* 
         * Example: if your permalink structure is /blog/, 
         * then your links will be: false->/news/, true->/blog/news/.
         */
        'with_front'        => false // Defaults to |true|
    ),

    //
    //'taxonomies' => array('project-category'),

    //
    'has_archive'       => false, // Set to false hides Archive Pages

    'publicly_queryable' => false, // Set to false hides Single Pages

    // Has to be true to make use of the the Gutenberg editor
    'show_in_rest'      => true,

    //
    'show_in_admin_bar' => false,

    //
    'show_in_nav_menus' => false,

    //
    'has_archive'       => false,

    // A URL slug for your taxonomy
    'query_var'         => false
  );

  // Call the actual WordPress function
  register_post_type( 'awsm-project', $args);

  register_taxonomy(
  
    // Give it a name, it will be used as slug as well
    'awsm-projects-category',

    // Connect it to the custom 'project' type
    'awsm-project',

    //
    array(
    'label' => __( 'Categories' ), 
    'rewrite' => array( 'slug' => 'project-category' ),
    'hierarchical' => true,
        )

  );
