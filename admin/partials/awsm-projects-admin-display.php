<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.1
 *
 * @package    Awsm_Projects
 * @subpackage Awsm_Projects/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
