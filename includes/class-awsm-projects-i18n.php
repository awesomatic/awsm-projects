<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       awesomatic.nl
 * @since      0.1
 *
 * @package    Awsm_Projects
 * @subpackage Awsm_Projects/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1
 * @package    Awsm_Projects
 * @subpackage Awsm_Projects/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Projects_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'awsm-projects',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
